import { BlogpagePage } from './app.po';

describe('blogpage App', () => {
  let page: BlogpagePage;

  beforeEach(() => {
    page = new BlogpagePage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
